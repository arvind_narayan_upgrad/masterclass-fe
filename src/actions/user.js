import getUserDataApi from '../api/user'
import humps from 'humps'
export const SET_TOKENS = Symbol('SET_TOKENS')
export const SET_USER_DATA = Symbol('SET_USER_DATA')

export function getUserData () {
  return (dispatch) => {
    return getUserDataApi().then((res) => {
      dispatch({
        type: 'SET_USER_DATA',
        payload: humps.camelizeKeys(res)
      })
    })
  }
}
