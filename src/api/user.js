import axios from 'axios'
import humps from 'humps'
import config from 'config'
import cookieCutter from 'cookie-cutter'

export const authAPi = axios.create({
  baseURL: config.auth_api,
  timeout: 0
})

authAPi.interceptors.request.use((c) => {
  const newConfig = { ...c }
  newConfig.headers['auth-token'] = cookieCutter.get(
    `upgrad-auth-token.${config.app_env}`
  )
  newConfig.headers.sessionId = cookieCutter.get(
    `upgrad-sessionId.${config.app_env}`
  )
  return newConfig
})

const getUserData = () => {
  const path = `/apis/v2/users/`
  return authAPi.get(path).then((res) => {
    return humps.camelizeKeys(res.data)
  })
}

export default getUserData
