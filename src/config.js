export default {
  app_env: process.env.APP_ENV,
  mode: process.env.MODE,
  auth_api: process.env.AUTH_API_ENDPOINT,
  clevertapId: process.env.clevertapId,
  videoAccountId: process.env.videoAccountId
}
