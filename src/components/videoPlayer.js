import React, { useState, useEffect, useRef } from 'react'
import PropTypes from 'prop-types'
import cx from 'classnames'
import brightcovePlayerLoader from '@brightcove/player-loader'
import config from 'config'

const Player = ({ id }) => {
  const [scriptLoad, setScriptLoad] = useState(false)
  const [failed, setFailed] = useState(false)
  const playerRef = useRef(null)
  useEffect(() => {
    if (!scriptLoad) {
      const accountId = config.videoAccountId
      brightcovePlayerLoader({
        refNode: playerRef.current,
        accountId: accountId,
        videoId: id
      })
        .then(function () {
          // const elem = playerRef.current.getElementsByClassName('video-js')[0];
          // const player = window.videojs.getPlayer(elem);
          setScriptLoad(true)
        })
        .catch(function (err) {
          setFailed(true)
          // eslint-disable-next-line
          console.error(`Video Load Error for vide ${id} `, err);
        })
    }
  }, [])

  return (
    <div className={cx('videoContainer', 'videoContent')}>
      <div ref={playerRef} />
      {!scriptLoad && <div className={cx('tagLoader', 'preload')} />}
      {failed && (
        <div className={'failure'}>
          <h2>Failed to load video</h2>
        </div>
      )}
    </div>
  )
}

Player.propTypes = {
  id: PropTypes.any,
  showTranscripts: PropTypes.bool,
  element: PropTypes.object
}

export default Player

/* eslint-disable */

var responseEdited = '';
const Transcripts = (elem, player, element) => {
    player.ready(() => {
      let regex = /\d\d:\d\d\.\d\d\d\s+-->\s+\d\d:\d\d\.\d\d\d.*\n/gi,
        spacer,
        url,
        // +++ Create button and place in player +++
        downloadBtn = document.createElement('button');
      downloadBtn.innerHTML = 'Download Transcript';
      downloadBtn.id = 'download-button';
      // Get a handle on the spacer element

      spacer = player.controlBar.customControlSpacer.el();
      // Set the content of the spacer to be right justified
      spacer.setAttribute('style', 'justify-content: flex-end;');
      // Place the new element in the spacer
      spacer.appendChild(downloadBtn);
      downloadBtn.addEventListener('click', () => {
        // +++ Call download function +++
        download(responseEdited, 'transcript.txt', 'text/plain');
      });

      player.on('loadstart', function () {
        var colonLocation,
          // +++ Retrieve the URL from the text track, which is in this case a WebVTT caption file +++
          // +++ Call the getFile function to get the actual file from the URL +++
          chosenTrack = player.mediainfo.textTracks.filter(function (track) {
            return track.kind === 'captions' || track.kind === 'subtitles';
          });
        url = chosenTrack[0] ? chosenTrack[0].src : null;
        if (url) {
          // Make URL for text track protocol relative (no http or https)
          colonLocation = url.indexOf(':');
          url = url.substr(colonLocation + 1);
          getFile(url, function (response) {
            if (response) {
              responseEdited = response.replace(regex, '');
              responseEdited = responseEdited.replace('WEBVTT', '');
              responseEdited = responseEdited.replace(
                'X-TIMESTAMP-MAP=LOCAL:00:00:00.000,MPEGTS:0',
                ''
              );
              if (element) element.current.innerText = responseEdited;
            }
          });
        } else {
          responseEdited = 'No transcription found';
          if (element) element.current.innerText = responseEdited;
        }
      });
    });
  },
  getFile = (url, callback) => {
    var httpRequest = new XMLHttpRequest(),
      response,
      // response handler
      getResponse = function () {
        try {
          if (httpRequest.readyState === 4) {
            if (httpRequest.status === 200) {
              response = httpRequest.responseText;
              // some API requests return '{null}' for empty responses - breaks JSON.parse
              if (response === '{null}') {
                response = null;
              }
              // return the response
              callback(response);
            } else {
              callback(null);
            }
          }
        } catch (e) {
          callback(null);
        }
      };
    /**
     * set up request data
     */
    // set response handler
    httpRequest.onreadystatechange = getResponse;
    // open the request
    httpRequest.open('GET', url);
    // open and send request
    httpRequest.send();
  },
  download = (data, strFileName, strMimeType) => {
    var self = window, // this script is only for browsers anyway...
      defaultMime = 'application/octet-stream', // this default mime also triggers iframe downloads
      mimeType = strMimeType || defaultMime,
      payload = data,
      url = !strFileName && !strMimeType && payload,
      anchor = document.createElement('a'),
      toString = function (a) {
        return String(a);
      },
      myBlob = self.Blob || self.MozBlob || self.WebKitBlob || toString,
      fileName = strFileName || 'download',
      blob,
      reader;
    myBlob = myBlob.call ? myBlob.bind(self) : Blob;

    if (String(this) === 'true') {
      //reverse arguments, allowing download.bind(true, "text/xml", "export.xml") to act as a callback
      payload = [payload, mimeType];
      mimeType = payload[0];
      payload = payload[1];
    }

    if (url && url.length < 2048) {
      // if no filename and no mime, assume a url was passed as the only argument
      fileName = url.split('/').pop().split('?')[0];
      anchor.href = url; // assign href prop to temp anchor
      if (anchor.href.indexOf(url) !== -1) {
        // if the browser determines that it's a potentially valid url path:
        var ajax = new XMLHttpRequest();
        ajax.open('GET', url, true);
        ajax.responseType = 'blob';
        ajax.onload = function (e) {
          download(e.target.response, fileName, defaultMime);
        };
        setTimeout(function () {
          ajax.send();
        }, 0); // allows setting custom ajax headers using the return:
        return ajax;
      } // end if valid url?
    } // end if url?

    //go ahead and download dataURLs right away
    if (/^data:([\w+-]+\/[\w+.-]+)?[,;]/.test(payload)) {
      if (payload.length > 1024 * 1024 * 1.999 && myBlob !== toString) {
        payload = dataUrlToBlob(payload);
        mimeType = payload.type || defaultMime;
      } else {
        return navigator.msSaveBlob // IE10 can't do a[download], only Blobs:
          ? navigator.msSaveBlob(dataUrlToBlob(payload), fileName)
          : saver(payload); // everyone else can save dataURLs un-processed
      }
    } else {
      //not data url, is it a string with special needs?
      if (/([\x80-\xff])/.test(payload)) {
        var i = 0,
          tempUiArr = new Uint8Array(payload.length),
          mx = tempUiArr.length;
        for (i; i < mx; ++i) tempUiArr[i] = payload.charCodeAt(i);
        payload = new myBlob([tempUiArr], { type: mimeType });
      }
    }
    blob =
      payload instanceof myBlob
        ? payload
        : new myBlob([payload], { type: mimeType });

    function dataUrlToBlob(strUrl) {
      var parts = strUrl.split(/[:;,]/),
        type = parts[1],
        indexDecoder = strUrl.indexOf('charset') > 0 ? 3 : 2,
        decoder = parts[indexDecoder] == 'base64' ? atob : decodeURIComponent,
        binData = decoder(parts.pop()),
        mx = binData.length,
        i = 0,
        uiArr = new Uint8Array(mx);

      for (i; i < mx; ++i) uiArr[i] = binData.charCodeAt(i);

      return new myBlob([uiArr], { type: type });
    }

    function saver(url, winMode) {
      if ('download' in anchor) {
        //html5 A[download]
        anchor.href = url;
        anchor.setAttribute('download', fileName);
        anchor.className = 'download-js-link';
        anchor.innerHTML = 'downloading...';
        anchor.style.display = 'none';
        anchor.addEventListener('click', function (e) {
          e.stopPropagation();
          this.removeEventListener('click', arguments.callee);
        });
        document.body.appendChild(anchor);
        setTimeout(function () {
          anchor.click();
          document.body.removeChild(anchor);
          if (winMode === true) {
            setTimeout(function () {
              self.URL.revokeObjectURL(anchor.href);
            }, 250);
          }
        }, 66);
        return true;
      }

      // handle non-a[download] safari as best we can:
      if (
        /(Version)\/(\d+)\.(\d+)(?:\.(\d+))?.*Safari\//.test(
          navigator.userAgent
        )
      ) {
        if (/^data:/.test(url))
          url = 'data:' + url.replace(/^data:([\w\/\-\+]+)/, defaultMime);
        if (!window.open(url)) {
          // popup blocked, offer direct download:
          if (
            confirm(
              'Displaying New Document\n\nUse Save As... to download, then click back to return to this page.'
            )
          ) {
            location.href = url;
          }
        }
        return true;
      }

      //do iframe dataURL download (old ch+FF):
      var f = document.createElement('iframe');
      document.body.appendChild(f);

      if (!winMode && /^data:/.test(url)) {
        // force a mime that will download:
        url = 'data:' + url.replace(/^data:([\w\/\-\+]+)/, defaultMime);
      }
      f.src = url;
      setTimeout(function () {
        document.body.removeChild(f);
      }, 333);
    } //end saver

    if (navigator.msSaveBlob) {
      // IE10+ : (has Blob, but not a[download] or URL)
      return navigator.msSaveBlob(blob, fileName);
    }

    if (self.URL) {
      // simple fast and modern way using Blob and URL:
      saver(self.URL.createObjectURL(blob), true);
    } else {
      // handle non-Blob()+non-URL browsers:
      if (typeof blob === 'string' || blob.constructor === toString) {
        try {
          return saver('data:' + mimeType + ';base64,' + self.btoa(blob));
        } catch (y) {
          return saver('data:' + mimeType + ',' + encodeURIComponent(blob));
        }
      }

      // Blob but not URL support:
      reader = new FileReader();
      reader.onload = function (e) {
        saver(this.result);
      };
      reader.readAsDataURL(blob);
    }
    return true;
  };
