const initialState = {
  authToken: undefined,
  sessionId: undefined,
  loggedIn: false,
  userData: undefined
}

export default function user (state = initialState, action) {
  switch (action.type) {
    case 'SET_TOKENS':
      return {
        ...state,
        authToken: action.payload.authToken,
        sessionId: action.payload.sessionId
      }

    case 'SET_USER_DATA':
      return {
        ...state,
        userData: action.payload,
        loggedIn: true
      }

    default:
      return state
  }
}
