import React from 'react'
import { useSelector } from 'react-redux'
import UpgradLogo from '../assets/upgrad'
import Close from '../assets/close'
import Select from 'react-select'
import { motion } from 'framer-motion'
import VideoPlayer from '../src/components/videoPlayer'
import { useRouter } from 'next/router'
import Categories from '../assets/categories'
import cx from 'classnames'

const Index = (props) => {
  const [loading, setLoading] = React.useState(true)
  const [data, setData] = React.useState([])
  const [videoSel, setVideoSel] = React.useState()
  const loggedIn = useSelector((s) => s.user.loggedIn)
  const router = useRouter()
  const { play } = router.query
  const [floatOptionsShow, setFloatOptionsShow] = React.useState(true)
  const [floatCatShow, setFloatCatShow] = React.useState(false)

  // Load initial data here
  React.useEffect(() => {
    // listApiCall().then(({ data }) => {
    //   setData(data);
    //   setLoading(false);
    // });

    setTimeout(() => {
      setData(dummyData)
      setLoading(false)
    }, 500)
  }, [])

  React.useEffect(() => {
    if (play) {
      setVideoSel(play)
    }

    window.addEventListener('scroll', scrollEvent, false)

    return () => window.removeEventListener('scroll', scrollEvent)
  }, [])

  var timer = null
  const scrollEvent = () => {
    if (floatOptionsShow) {
      setFloatOptionsShow(false)
    }

    if (timer !== null) {
      clearTimeout(timer)
    }
    timer = setTimeout(() => {
      if (!floatCatShow) {
        setFloatOptionsShow(true)
      }
    }, 100)
  }

  const onTileClick = (id) => {
    setFloatOptionsShow(false)
    if (loggedIn) {
      setVideoSel(id)
      router.push(`/?play=${id}`, undefined, { shallow: true })
    } else {
      // set token in local manually
      window.open(
        `http://accounts.upgrad.com?redirect=http://localhost:3000?play=${id}`
      )
    }
  }

  const closeModal = (e) => {
    router.push(`/`, undefined, { shallow: true })
    setVideoSel(undefined)
  }

  const options = [
    { value: 'all', label: 'All(55)' },
    { value: 'Jargon Kumar', label: 'Jargon Kumar (5)' },
    {
      value: 'Make your career covid ready',
      label: 'Make your career covid ready (5)'
    },
    {
      value: 'Softskills you cannot miss',
      label: 'Softskills you cannot miss (12)'
    },
    {
      value: 'Standup book #lifelonglearning',
      label: 'Standup book #lifelonglearning (15)'
    },
    {
      value: 'Shortgyaan',
      label: 'Shortgyaan (8)'
    },
    {
      value: 'Skill Everyday',
      label: 'Skill Everyday (10)'
    }
  ]

  const toggleMobCat = (bool) => {
    if (bool) {
      setFloatCatShow(true)
      setFloatOptionsShow(false)
    } else {
      setFloatCatShow(false)
      setFloatOptionsShow(true)
    }
  }

  return (
    <>
      <header className='title'>
        <div className='container'>
          <UpgradLogo />
          <span> MasterClass</span>
        </div>
      </header>
      <header className='nav'>
        <div className='container'>
          <h3>Introducing the all new,</h3>
          <h1>Masterclass</h1>
        </div>
      </header>
      <div className='feed'>
        <div className='container'>
          <div className='feedTitle'>
            <h4>Videos For You</h4>
            <div className='sel'>
              <label>Category</label>
              <Select defaultValue={options[0]} options={options} />
            </div>
          </div>
          <div className='feedContent'>
            {!loading
              ? data.map((d) => (
                <VideoTile
                  key={d.id}
                  tag={d.tag}
                  name={d.name}
                  title={d.name}
                  author={d.author}
                  views={d.views}
                  time={d.length}
                  posterImg={d.posterImage}
                  onSelect={() => onTileClick(d.videoId)}
                />
              ))
              : [1, 2, 3, 4, 5, 6].map((l) => (
                <div key={`${l}loader`} className='skeletonLoader' />
              ))}
          </div>
        </div>
      </div>
      <footer>
        <a href='https://upgrad.com' target='_blank'>
          <UpgradLogo />
        </a>
        <span>
          2015 - 2021 upGrad Education Private Limited. All rights reserved
        </span>
      </footer>
      {videoSel && (
        <motion.div
          initial={{ opacity: 0.4, y: '0%' }}
          animate={{ opacity: 1 }}
          exit={{ opacity: 0.4 }}
          transition={{ duration: 0.25 }}
          className='videoModal'
        >
          <button onClick={(e) => closeModal(e)} className='modalClose'>
            <Close />
          </button>
          <VideoPlayer id={videoSel} />
        </motion.div>
      )}

      {floatOptionsShow && (
        <motion.button
          initial={{ x: '-50%', y: '200%', opacity: 0.5 }}
          animate={{ x: '-50%', y: '0%', opacity: 1 }}
          exit={{ x: '-50%', y: '200%', opacity: 0.5 }}
          transition={{ duration: 0.2 }}
          className='categoriesFloat'
          id='fabCatButton'
          onClick={() => toggleMobCat(true)}
        >
          <Categories />
          Browse Categories
        </motion.button>
      )}

      {floatCatShow && (
        <>
          <motion.div
            initial={{ x: '-50%', y: '200%', opacity: 0.5 }}
            animate={{ x: '-50%', y: '0%', opacity: 1 }}
            exit={{ x: '-50%', y: '200%', opacity: 0.5 }}
            transition={{ duration: 0.2 }}
            className='mobCatList'
          >
            {options.map((o, i) => (
              <span
                className={cx('catItem', {
                  active: i === 0
                })}
              >
                {o.label}
              </span>
            ))}
          </motion.div>
          <div className='backDrop' onClick={() => toggleMobCat(false)} />
        </>
      )}
    </>
  )
}

export default Index

const VideoTile = ({
  id,
  tag,
  author,
  title,
  views,
  time,
  posterImg,
  onSelect
}) => {
  const t = new Date(time * 1000).toISOString().substr(14, 5)

  return (
    <motion.div
      className='videoTile'
      initial={{ opacity: 0.4, y: '15%' }}
      animate={{ opacity: 1, y: '0%' }}
      exit={{ opacity: 0.4, y: '-15%' }}
      transition={{ duration: 0.25 }}
      style={{
        backgroundImage: `linear-gradient(180deg, #0000 25%, #000 100%), url(${posterImg})`
      }}
      onClick={() => onSelect()}
    >
      <div className='tileHead'>
        <span className='time'>{t}</span>
      </div>
      <div className='tileData'>
        <span className='tag'>{tag}</span>
        <span className='author'>{author}</span>
        <h4 className='title'>{title}</h4>
      </div>
      <div className='tileFoot'>
        <span className='views'>{`${views} views`}</span>
      </div>
    </motion.div>
  )
}

const dummyData = [
  {
    id: 'ak001',
    tag: 'Skills Everyday',
    name: "Eat That Frog' by Brian Tracy",
    author: 'Vicky Kaushal',
    views: 212,
    length: 234,
    videoId: 6227640261001,
    posterImage:
      'https://images.pexels.com/photos/5721449/pexels-photo-5721449.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260'
  },
  {
    id: 'ak002',
    tag: 'Skills Everyday',
    name: 'Steps To Be Self Aware at Workplace',
    author: 'Ravish Kumar',
    views: 112,
    length: 334,
    videoId: 5294633272001,
    posterImage:
      'https://images.pexels.com/photos/5474031/pexels-photo-5474031.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260'
  },
  {
    id: 'ak003',
    tag: 'Skills Everyday',
    name: `How to answer ‘Tell me about yourself`,
    author: 'Harsha Bhogle',
    views: 312,
    length: 134,
    videoId: 6227640261001,
    posterImage:
      'https://images.pexels.com/photos/2915216/pexels-photo-2915216.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260'
  },
  {
    id: 'ak004',
    tag: 'Skills Everyday',
    name: "Eat That Frog' by Brian Tracy",
    author: 'Vicky Kaushal',
    views: 212,
    length: 234,
    videoId: 6227640261001,
    posterImage:
      'https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260'
  },
  {
    id: 'ak005',
    tag: 'Skills Everyday',
    name: 'Steps To Be Self Aware at Workplace',
    author: 'Ravish Kumar',
    views: 112,
    length: 334,
    videoId: 5294633272001,
    posterImage:
      'https://images.pexels.com/photos/5198239/pexels-photo-5198239.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260'
  },
  {
    id: 'ak006',
    tag: 'Skills Everyday',
    name: `How to answer ‘Tell me about yourself`,
    author: 'Harsha Bhogle',
    views: 312,
    length: 134,
    videoId: 6227640261001,
    posterImage:
      'https://images.pexels.com/photos/3772618/pexels-photo-3772618.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260'
  },
  {
    id: 'a001',
    tag: 'Skills Everyday',
    name: "Eat That Frog' by Brian Tracy",
    author: 'Vicky Kaushal',
    views: 212,
    length: 234,
    videoId: 6227640261001,
    posterImage:
      'https://images.pexels.com/photos/5721449/pexels-photo-5721449.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260'
  },
  {
    id: 'a002',
    tag: 'Skills Everyday',
    name: 'Steps To Be Self Aware at Workplace',
    author: 'Ravish Kumar',
    views: 112,
    length: 334,
    videoId: 5294633272001,
    posterImage:
      'https://images.pexels.com/photos/5474031/pexels-photo-5474031.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260'
  },
  {
    id: 'a003',
    tag: 'Skills Everyday',
    name: `How to answer ‘Tell me about yourself`,
    author: 'Harsha Bhogle',
    views: 312,
    length: 134,
    videoId: 6227640261001,
    posterImage:
      'https://images.pexels.com/photos/2915216/pexels-photo-2915216.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260'
  },
  {
    id: 'a004',
    tag: 'Skills Everyday',
    name: "Eat That Frog' by Brian Tracy",
    author: 'Vicky Kaushal',
    views: 212,
    length: 234,
    videoId: 6227640261001,
    posterImage:
      'https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260'
  },
  {
    id: 'a005',
    tag: 'Skills Everyday',
    name: 'Steps To Be Self Aware at Workplace',
    author: 'Ravish Kumar',
    views: 112,
    length: 334,
    videoId: 5294633272001,
    posterImage:
      'https://images.pexels.com/photos/5198239/pexels-photo-5198239.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260'
  },
  {
    id: 'a006',
    tag: 'Skills Everyday',
    name: `How to answer ‘Tell me about yourself`,
    author: 'Harsha Bhogle',
    views: 312,
    length: 134,
    videoId: 6227640261001,
    posterImage:
      'https://images.pexels.com/photos/3772618/pexels-photo-3772618.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260'
  }
]
