import { Helmet } from 'react-helmet'
import withRedux from 'next-redux-wrapper'
import { Provider } from 'react-redux'
import { withRouter } from 'next/router'
import App from 'next/app'
import cookieCutter from 'cookie-cutter'
import config from 'config'
import createStore from 'store/createStore'
import { getUserData } from '../src/actions/user'
import { initialize } from '../utils/clevertap'
import '../styles/global.scss'
import '../styles/index.scss'
class MyApp extends App {
  componentDidMount () {
    const authToken = cookieCutter.get(`upgrad-auth-token.${config.app_env}`)
    const sessionId = cookieCutter.get(`upgrad-sessionId.${config.app_env}`)

    if (authToken?.length > 0) {
      this.props.store.dispatch({
        type: 'SET_TOKENS',
        payload: {
          authToken,
          sessionId
        }
      })
      this.props.store.dispatch(getUserData())
    }

    initialize({ CLEVERTAP_ID: config.clevertapId })
  }

  render () {
    const { Component, pageProps, router, store } = this.props
    const title = 'upGrad | Masterclass'
    return (
      <>
        <Helmet>
          <meta charset='utf-8' />
          <title>{title}</title>
          <link rel='icon' href='https://upgrad.com/favicon.ico' />
          <meta name='viewport' content='width=device-width, initial-scale=1' />
          <meta property='og:title' content={title} />
          <link rel='canonical' href='https://lite.upgrad.com' />
          <meta name='viewport' content='width=device-width, initial-scale=1' />
          <meta name='theme-color' content='#5c6b78' />
          <meta name='apple-mobile-web-app-title' content='upGrad' />
          <meta name='apple-mobile-web-app-capable' content='yes' />
          <meta name='apple-mobile-web-app-status-bar-style' content='black' />
          <meta name='mobile-web-app-capable' content='yes' />
          <meta http-equiv='ScreenOrientation' content='autoRotate:disabled' />
          <meta
            name='description'
            content="Enrol in upGrad's online courses to gain certification in data science, digital marketing, product management, machine learning, software development, and more. Get the unique upGrad experience - learn through a content co-developed by academia and industry experts &amp; get a dedicated mentor and career support."
          />
          <meta
            name='twitter:image'
            content='https://ik.imagekit.io/upgrad1/marketing-platform-assets/meta-images/home.jpg'
          />
          <meta name='twitter:site' content='@upGrad_Edu' />
          <meta name='author' content='upGrad' />
          <meta
            name='title'
            property='og:title'
            content='Online Courses &amp; Education Programs  | upGrad'
          />
          <meta
            name='description'
            property='og:description'
            content="Enrol in upGrad's online courses to gain certification in data science, digital marketing, product management, machine learning, software development, and more. Get the unique upGrad experience - learn through a content co-developed by academia and industry experts &amp; get a dedicated mentor and career support."
          />
          <meta
            name='image'
            property='og:image'
            content='https://ik.imagekit.io/upgrad1/marketing-platform-assets/meta-images/home.jpg'
          />
          <meta name='og:type' property='og:type' content='website' />
          <meta name='og:site_name' property='og:site_name' content='upGrad' />
          <meta
            name='twitter:title'
            content='Online Courses &amp; Education Programs  | upGrad'
          />
          <meta
            name='twitter:description'
            content="Enrol in upGrad's online courses to gain certification in data science, digital marketing, product management, machine learning, software development, and more. Get the unique upGrad experience - learn through a content co-developed by academia and industry experts &amp; get a dedicated mentor and career support."
          />
          <meta
            name='twitter:image'
            content='https://ik.imagekit.io/upgrad1/marketing-platform-assets/meta-images/home.jpg'
          />
          <meta name='twitter:card' content='summary_large_image' />
          <link rel='preconnect' href='https://fonts.gstatic.com' />
          <link
            href='https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700;900&display=swap'
            rel='stylesheet'
          />
          <link
            rel='icon'
            type='image/x-icon'
            href='https://upgrad.com/favicon.ico'
          />
          <link rel='apple-touch-icon' href='https://upgrad.com/favicon.ico' />
          <link
            rel='apple-touch-icon-precomposed'
            sizes='57x57'
            href='https://www.upgrad.com/apple-touch-icon-57x57.png'
          />
          <link
            rel='apple-touch-icon-precomposed'
            sizes='72x72'
            href='https://www.upgrad.com/apple-touch-icon-72x72.png'
          />
          <link
            rel='apple-touch-icon-precomposed'
            sizes='114x114'
            href='https://www.upgrad.com/apple-touch-icon-114x114.png'
          />
          <link
            rel='apple-touch-icon-precomposed'
            sizes='120x120'
            href='https://www.upgrad.com/apple-touch-icon-120x120.png'
          />
          <link
            rel='apple-touch-icon-precomposed'
            sizes='144x144'
            href='https://www.upgrad.com/apple-touch-icon-144x144.png'
          />
          <link
            rel='apple-touch-icon-precomposed'
            sizes='152x152'
            href='https://www.upgrad.com/apple-touch-icon-152x152.png'
          />
        </Helmet>
        <Provider store={store}>
          <Component router={router} {...pageProps} />
        </Provider>
      </>
    )
  }
}

export default withRedux(createStore)(withRouter(MyApp))
