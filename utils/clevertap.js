/* eslint-disable no-param-reassign */
import platform from 'platform'
import config from 'config'
import store from '../src/store/createStore'
import cookieCutter from 'cookie-cutter'

export function isClevertapAvailable () {
  return typeof window.clevertap !== 'undefined'
}

export function initialize ({ CLEVERTAP_ID }) {
  if (!CLEVERTAP_ID) return
  window.clevertap = {
    event: [],
    profile: [],
    account: [],
    onUserLogin: [],
    notifications: []
  }
  window.clevertap.account.push({ id: CLEVERTAP_ID });
  (() => {
    const wzrk = document.createElement('script')
    wzrk.type = 'text/javascript'
    wzrk.async = true
    wzrk.src = `${
      document.location.protocol === 'https:'
        ? 'https://d2r1yp2w7bby2u.cloudfront.net'
        : 'http://static.clevertap.com'
    }/js/a.js`
    const s = document.getElementsByTagName('script')[0]
    s.parentNode.insertBefore(wzrk, s)
  })()
}

const clean = (obj) => {
  Object.keys(obj).forEach((key) => {
    if (obj[key] === null || obj[key] === '' || obj[key] === undefined) {
      delete obj[key]
    }
  })

  return obj
}

export function LogEvent (name, properties = {}) {
  const os = platform.os || {}

  const baseProperties = {
    client_os: os.family,
    client_version: os.version,
    user_agent: platform.ua,
    courseid: '',
    width: `${window.innerWidth}px`,
    height: `${window.innerHeight}px`,
    platform_version: platform.version,
    browser: platform.name,
    manfacturer: platform.manufacturer,
    device_model: platform.description,
    userid: '',
    course_name: '',
    page_url: window.location.href,
    page_title: '',
    'User Progress': '',
    app_session: cookieCutter.get(`upgrad-sessionId.${config.app_env}`),
    Time_stamp: new Date()
  }

  const { user } = store.getState()

  if (!isClevertapAvailable()) return

  if (!name) return

  try {
    if (!properties || !typeof properties === 'object') {
      properties = {}
    }

    properties = {
      ...properties,
      ...clean({
        ...baseProperties,
        userid: user.userData?.id || null
      })
    }
    window.clevertap.event.push(name, properties)
  } catch (e) {
    console.error(`Unable to track event ${name}`, e)
  }
}
